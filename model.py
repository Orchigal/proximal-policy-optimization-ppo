import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torch.nn.utils import weight_norm


class ActorCritic(nn.Module):

    def __init__(self):
        super(ActorCritic, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=8, stride=4)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=4, stride=2)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3, stride=1)
        self.lstm = nn.LSTMCell(6*6*64, 512)
        self.critic_linear = nn.Linear(512, 1)
        self.actor_linear = nn.Linear(512, 4)
        
        self._initialize_weights()

    def forward(self, inputs):
        inputs, (hx, cx) = inputs
        x = F.relu(self.conv1(inputs))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        hx, cx = self.lstm(x.view(x.size(0),-1), (hx, cx))
        x = hx

        return self.critic_linear(x), self.actor_linear(x), (hx,cx)
    
    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                weight_shape = list(m.weight.data.size())
                fan_in = np.prod(weight_shape[1:4])
                fan_out = np.prod(weight_shape[2:4]) * weight_shape[0]
                w_bound = np.sqrt(6. / (fan_in + fan_out))
                m.weight.data.uniform_(-w_bound, w_bound)
                m.bias.data.fill_(0)
            elif isinstance(m, nn.Linear):
                nn.init.kaiming_uniform(m.weight.data, mode='fan_in')
                m.bias.data.fill_(0)
                weight_norm(m)
            elif isinstance(m, nn.LSTMCell):
                m.bias_ih.data.fill_(0)
                m.bias_hh.data.fill_(0)